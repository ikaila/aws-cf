set -e

# archiving code
zip code.zip ./code.py

# updating s3 with updated code
# comment out the below line if you aws profile in the credentials file is not named this way
export AWS_PROFILE=dava
aws s3 sync . s3://anisoara-internship-materials/lambda

# validating template
CF_VALIDATION=$(aws cloudformation validate-template --template-url https://anisoara-internship-materials.s3.us-east-1.amazonaws.com/lambda/lambda.yaml)
echo $CF_VALIDATION

# extracting capabilities from validation
CAPABILITIES=$(echo $CF_VALIDATION | jq -r '.Capabilities[]')

# creating stack
aws cloudformation create-stack --stack-name anisoara-internship --template-url https://anisoara-internship-materials.s3.us-east-1.amazonaws.com/lambda/lambda.yaml --capabilities $CAPABILITIES
